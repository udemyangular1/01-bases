import { Component } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent  {

  heroes : string[] = ['Spiderman', 'Ironman', 'Hulk', 'Thor'];
  heroesBorrados: string[] = [];

  borrarHeroe(): void {
    
    let miHeroeBorrado: string = this.heroes.shift() || "";

    if(miHeroeBorrado !== "")
      this.heroesBorrados.push(miHeroeBorrado);
   
  }

}
