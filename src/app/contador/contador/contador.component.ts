import { Component } from "@angular/core";


@Component({
    selector: 'app-contador',
    template: `
        <h1>{{title}}</h1>
        <button (click)="acumular(base)">+ {{base}}</button>
        <span>{{numero}}</span>
        <button (click) ="acumular(-base)">- {{base}}</button>   
    `
})
export class ContadorComponent {

    title: string = 'Perro';
    numero: number = 0;
    base: number = 5;

    acumular(numero: number): number {
        return this.numero += numero;
    }

}