import { Injectable } from "@angular/core";
import { Personaje } from "../interfaces/dbz.interface";


@Injectable()
export class DbzService {

    private _personajes: Personaje[] = [
        {
          nombre: "Krilin",
          poder: 500
        },
        {
          nombre: "Goku",
          poder: 15000
        }
    ];

    get personajes(): Personaje[] {
        return [...this._personajes];
    }

    agregarPersonaje( personaje: Personaje ) {
        this._personajes.push(personaje);
    }

}