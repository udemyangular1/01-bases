import { Component } from '@angular/core';
import { Personaje } from '../interfaces/dbz.interface';
import { DbzService } from '../services/dbz.service';

@Component({
  selector: 'app-dbz',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent {  

  nuevo: Personaje = {
    nombre: 'Mi perro',
    poder: 120000
  }  
  
  constructor(private dbzService: DbzService) {}

}
